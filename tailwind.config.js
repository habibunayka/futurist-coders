

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './index.html',
    './src/**/*.{js,ts,jsx,tsx}',
  ],
  theme: {
    extend: {
      colors: {
        icon: '#D2824E',
        icon1:
        
        '#D2855F',

        primary: '#313338',
        secondary: '#2B2D31',
        darkPrimary: '#232428',
        darkSecondary: '#1E1F22',
        black: '#000000',
        white: '#ffffff',
        success: '#28a745',
        danger: '#dc3545',
        warning: '#ffc107',
        info: '#17a2b8',
      },
      fontFamily: {
        helvetica: ['Poppins', 'Arial', 'sans-serif'],
      },
    },
  },
  plugins: [],
}
