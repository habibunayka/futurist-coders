import React, { useState } from "react";
import { NavLink, useNavigate } from "react-router-dom";
import 'boxicons/css/boxicons.min.css';
import "../styles/css/custom.css";

function Sidebar() {
  const [isOpen, setIsOpen] = useState(false);
  const [hovered, setHovered] = useState(null);
  const navigate = useNavigate();

  const toggleMenu = () => {
    setIsOpen(!isOpen);
  };

  const handleMouseEnter = (item) => {
    setHovered(item);
  };

  const handleMouseLeave = () => {
    setHovered(null);
  };

  const handleClick = (path) => {
    navigate(path);
    window.location.reload(); // Reload the page
  };

  const activeClassName = "active"; // Add your active class styling here

  return (
    <div className="relative">
      {/* Hamburger Icon */}
      <div className={`fixed top-4 z-50 transition-transform bg-darkPrimary md:hidden ${isOpen ? "translate-x-full left-8" : "translate-x-0 left-4"}`}>
        <button
          onClick={toggleMenu}
          className="text-white focus:outline-none p-2"
        >
          <svg
            className="w-6 h-6"
            fill="none"
            stroke="currentColor"
            viewBox="0 0 24 24"
            xmlns="http://www.w3.org/2000/svg"
          >
            {isOpen ? (
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth={2}
                d="M6 18L18 6M6 6l12 12"
              />
            ) : (
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth={2}
                d="M4 6h16M4 12h16m-7 6h7"
              />
            )}
          </svg>
        </button>
      </div>

      {/* Sidebar Menu */}
      <div className={`fixed top-0 left-0 h-full bg-darkPrimary overflow-y-auto transition-transform transform ${isOpen ? "translate-x-0" : "-translate-x-full"} md:translate-x-0 md:relative md:w-[4.2rem] md:h-[98vh] z-40`}>
        <div className="scrollbar flex flex-col justify-start min-w-[4.2rem] max-w-[4.2rem]">
          <NavLink
            to="/"
            className={({ isActive }) => `sidebar-icon home ${isActive ? activeClassName : ''} bg-darkPrimary hover:bg-primary min-w-[3rem] max-w-[3rem] min-h-[3rem] max-h-[3rem] m-[1vh_auto] relative cursor-pointer flex items-center justify-center`}
            onMouseEnter={() => handleMouseEnter("Home")}
            onMouseLeave={handleMouseLeave}
            onClick={() => handleClick('/')}
          >
            <i className="bx bx-home text-4xl" style={{ color: '#D2824E' }}></i>
            {hovered === "Home" && (
              <span className="tooltip bg-primary text-white rounded-lg px-2 py-1 absolute top-1/2 left-full ml-2 transform -translate-y-1/2">
                Home
              </span>
            )}
          </NavLink>
          <div className="w-10 h-0.5 relative bg-primary m-[1.2vh_auto]"></div>
          <NavLink
            to="/chatbot"
            className={({ isActive }) => `sidebar-icon about ${isActive ? activeClassName : ''} bg-darkPrimary hover:bg-primary min-w-[3rem] max-w-[3rem] min-h-[3rem] max-h-[3rem] m-[1vh_auto] relative cursor-pointer flex items-center justify-center`}
            onMouseEnter={() => handleMouseEnter("ChatBot")}
            onMouseLeave={handleMouseLeave}
            onClick={() => handleClick('/chatbot')}
          >
            <i className="bx bx-bot text-4xl" style={{ color: '#D2824E' }}></i>
            {hovered === "ChatBot" && (
              <span className="tooltip bg-primary text-white rounded-lg px-2 py-1 absolute top-1/2 left-full ml-2 transform -translate-y-1/2">
                ChatBot
              </span>
            )}
          </NavLink>
          <div className="w-10 h-0.5 relative bg-primary m-[1.2vh_auto]"></div>
          <NavLink
            to="/articles"
            className={({ isActive }) => `sidebar-icon about ${isActive ? activeClassName : ''} bg-darkPrimary hover:bg-primary min-w-[3rem] max-w-[3rem] min-h-[3rem] max-h-[3rem] m-[1vh_auto] relative cursor-pointer flex items-center justify-center`}
            onMouseEnter={() => handleMouseEnter("Articles")}
            onMouseLeave={handleMouseLeave}
            onClick={() => handleClick('/articles')}
          >
            <i className="bx bx-book-content text-4xl" style={{ color: '#D2824E' }}></i>
            {hovered === "Articles" && (
              <span className="tooltip bg-primary text-white rounded-lg px-2 py-1 absolute top-1/2 left-full ml-2 transform -translate-y-1/2">
                Articles
              </span>
            )}
          </NavLink>
          <div className="w-10 h-0.5 relative bg-primary m-[1.2vh_auto]"></div>
          <NavLink
            to="/about"
            className={({ isActive }) => `sidebar-icon about ${isActive ? activeClassName : ''} bg-darkPrimary hover:bg-primary min-w-[3rem] max-w-[3rem] min-h-[3rem] max-h-[3rem] m-[1vh_auto] relative cursor-pointer flex items-center justify-center`}
            onMouseEnter={() => handleMouseEnter("About")}
            onMouseLeave={handleMouseLeave}
            onClick={() => handleClick('/about')}
          >
            <i className="bx bx-info-circle text-4xl" style={{ color: '#D2824E' }}></i>
            {hovered === "About" && (
              <span className="tooltip bg-primary text-white rounded-lg px-2 py-1 absolute top-1/2 left-full ml-2 transform -translate-y-1/2">
                About
              </span>
            )}
          </NavLink>
        </div>
      </div>
    </div>
  );
}

export default Sidebar;
