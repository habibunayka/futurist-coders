import React from 'react';
import { useParams } from 'react-router-dom';

const ArticleDetail = () => {
    const { articleId } = useParams();

    const articleData = {
        'understanding-social-media-algorithms': {
            title: 'Understanding Social Media Algorithms',
            image: 'https://quickframe.com/wp-content/uploads/2023/06/QF-Blog-How-Do-Social-Media-Algorithms-Work-in-2023-01.jpg', // Ganti dengan URL gambar yang relevan
            content: `
                <p>Explore the inner workings of social media algorithms and their impact on content visibility. Understanding these algorithms is key to mastering social media strategy.</p>
                
                <p>Social media algorithms are sophisticated systems designed to determine which posts appear in a user's feed based on various factors. They use machine learning and data analysis to enhance user experience by prioritizing content that is most relevant to each individual.</p>
                
                <h2>How Do Social Media Algorithms Work?</h2>
                
                <ol type="1">
                    <li><strong>User Engagement:</strong> Algorithms favor posts that generate high levels of interaction, such as likes, shares, comments, and reactions. The more engagement a post receives, the more likely it is to be seen by a wider audience.</li>
                    <li><strong>Content Relevance:</strong> The content's relevance to the user's interests and past behaviors plays a significant role. Algorithms assess the content type, keywords, and user preferences to determine its relevance.</li>
                    <li><strong>Timing:</strong> Recent and timely posts are often prioritized to keep the content fresh and up-to-date in users' feeds.</li>
                    <li><strong>Content Format:</strong> Different platforms may prioritize various content formats, such as videos, images, or text-based posts, based on user engagement and preferences.</li>
                    <li><strong>Personalization:</strong> Algorithms use data to personalize each user's experience, tailoring the feed to show content that aligns with their unique interests and behavior.</li>
                </ol>
                
                <h2>Implications for Content Creators</h2>
                
                <p>For content creators, understanding these factors is crucial for optimizing visibility and engagement. Strategies include focusing on high-quality, engaging content, utilizing relevant keywords, and posting during peak times to maximize reach.</p>
                
                <h2>Challenges and Considerations</h2>
                
                <p>While algorithms enhance user experience, they also present challenges. Issues such as echo chambers, where users are only exposed to similar viewpoints, and reduced content diversity can arise. Balancing personalization with diverse content is essential for a well-rounded social media strategy.</p>
            `
        },
        'the-role-of-data-privacy': {
            title: 'The Role of Data Privacy in Social Media',
            image: 'https://i0.wp.com/kliklegal.com/wp-content/uploads/2021/09/Perbedaan-Data-Privacy-dan-Data-Protection-serta-Pengaturannya-pada-RUU-Pelindungan-Data-Pribadi.jpg?fit=700%2C386&ssl=1', // Ganti dengan URL gambar yang relevan
            content: `
                <p>Discover the importance of data privacy in the realm of social media and learn how to safeguard your personal information online.</p>
                
                <p>Data privacy involves protecting personal information from unauthorized access, misuse, or breaches on social media platforms. With growing concerns over data security, it's crucial to understand and manage your privacy settings effectively.</p>
                
                <h2>Key Aspects of Data Privacy</h2>
                
                <ol>
                    <li><strong>Privacy Settings:</strong> Regularly adjust your privacy settings to control who can view your posts and personal information. Platforms offer various options to customize your visibility and data sharing preferences.</li>
                    <li><strong>Sensitive Information:</strong> Avoid sharing sensitive details such as your home address, financial information, or health records on social media. Be cautious about what you disclose publicly.</li>
                    <li><strong>Third-Party Applications:</strong> Review the permissions granted to third-party applications and services that may access your social media data. Limit access to only those applications you trust.</li>
                    <li><strong>Data Breaches:</strong> Stay informed about potential data breaches and security incidents affecting social media platforms. Take necessary steps to secure your accounts if a breach occurs.</li>
                </ol>
                
                <h2>Best Practices for Protecting Your Privacy</h2>
                
                <p>Regularly review and update your privacy settings, be mindful of the information you share, and stay informed about the data collection practices of social media platforms. Using strong, unique passwords and enabling two-factor authentication can further enhance your account security.</p>
                
                <h2>Regulatory Measures</h2>
                
                <p>Regulations such as the GDPR (General Data Protection Regulation) in Europe and CCPA (California Consumer Privacy Act) in the U.S. are designed to protect users' data privacy. Familiarize yourself with these regulations to understand your rights and the protections available to you.</p>
            `
        },
        'the-evolution-of-social-media-platforms': {
            title: 'The Evolution of Social Media Platforms',
            image: 'https://www.socialchamp.io/wp-content/uploads/2023/11/Content-Blog-Banner_Q3-2023_1125x600_73-Social-Media-Platforms.png', // Ganti dengan URL gambar yang relevan
            content: `
                <p>Explore the evolution of social media platforms from their inception to the present day and understand how they have transformed over time.</p>
                
                <p>Social media platforms have undergone significant evolution, from early text-based forums to today's multimedia-rich networks. Each phase of this evolution has introduced new features and technologies, shaping the way we interact online.</p>
                
                <h2>Early Social Media</h2>
                
                <p>Early platforms like Friendster and MySpace were pioneers in social networking, focusing on user profiles and connections. These platforms allowed users to create personal profiles, connect with friends, and share updates, laying the groundwork for modern social media.</p>
                
                <h2>Modern Social Media</h2>
                
                <p>Contemporary platforms such as Facebook, Twitter, Instagram, and TikTok offer diverse features including multimedia sharing, live streaming, and algorithm-driven feeds. These advancements have revolutionized online interactions and content consumption.</p>
                
                <h2>Future Trends</h2>
                
                <p>Looking ahead, social media platforms are expected to integrate advanced technologies such as augmented reality (AR), virtual reality (VR), and artificial intelligence (AI). These innovations will further enhance user experiences and reshape the way we connect and share content.</p>
                
                <p>Understanding the evolution of social media provides valuable insights into current trends and future developments, helping users and creators adapt to the ever-changing digital landscape.</p>
            `
        }
    };
    

    const article = articleData[articleId] || {};

    return (
        <div className="bg-darkPrimary text-white min-h-screen py-12 px-4 -my-3 mx-2">
            <div className="container mx-auto max-w-4xl">
                {article ? (
                    <>
                        <img
                            src={article.image}
                            alt={article.title}
                            className="w-full h-72 object-cover rounded-lg mb-6"
                        />
                        <h1 className="text-4xl font-bold mb-6">{article.title}</h1>
                        <div
                            className="prose prose-lg prose-light text-gray-300 space-y-4"
                            dangerouslySetInnerHTML={{ __html: article.content }}
                        />
                    </>
                ) : (
                    <p className="text-center text-lg">Article not found</p>
                )}
            </div>
        </div>
    );
};

export default ArticleDetail;
