import React, { useState, useEffect, useRef } from 'react';
import '../styles/css/custom.css';

const ChatBot = () => {
    const [messages, setMessages] = useState([]);
    const [options, setOptions] = useState([
        'What is social media?',
        'How has social media evolved?',
        'What are the benefits of social media?',
        'What are the risks of social media?',
        'How can I use social media effectively?',
        'What are the latest trends in social media?',
    ]);
    const [subOptions, setSubOptions] = useState([]);
    const [isCooldown, setIsCooldown] = useState(false);
    const [isTyping, setIsTyping] = useState(false);

    const messagesEndRef = useRef(null);

    const scrollToBottom = () => {
        messagesEndRef.current?.scrollIntoView({ behavior: 'smooth' });
    };

    useEffect(() => {
        scrollToBottom();
    }, [messages]);

    const handleSelectOption = (option) => {
        if (isCooldown) return;

        setMessages(prevMessages => [
            ...prevMessages,
            { text: option, isUser: true }
        ]);

        if (option === 'What is social media?') {
            setSubOptions([
                'Definition of social media',
                'Examples of social media platforms'
            ]);
            return;
        }

        if (option === 'How has social media evolved?') {
            setSubOptions([
                'History of social media',
                'Key milestones in social media evolution',
                'Recent developments in social media'
            ]);
            return;
        }

        if (option === 'What are the benefits of social media?') {
            setSubOptions([
                'Personal benefits',
                'Professional benefits'
            ]);
            return;
        }

        if (option === 'What are the risks of social media?') {
            setSubOptions([
                'Privacy concerns',
                'Cyberbullying and misinformation'
            ]);
            return;
        }

        if (option === 'How can I use social media effectively?') {
            setSubOptions([
                'Creating engaging content',
                'Managing your online presence'
            ]);
            return;
        }

        if (option === 'What are the latest trends in social media?') {
            setSubOptions([
                'Short-form videos',
                'Augmented reality'
            ]);
            return;
        }

        if (subOptions.length > 0) {
            setIsTyping(true);
            setTimeout(() => {
                setIsTyping(false);
                const botResponse = getBotResponse(option);
                setMessages(prevMessages => [
                    ...prevMessages,
                    { text: botResponse, isUser: false }
                ]);
                setSubOptions([]);
            }, 1000);
        }

        setIsCooldown(true);
        setTimeout(() => {
            setIsCooldown(false);
        }, 2000);
    };

    const getBotResponse = (userInput) => {
        switch (userInput) {
            case 'Definition of social media':
                return 'Social media refers to websites and applications that allow users to create and share content.';
            case 'Examples of social media platforms':
                return 'Examples include Facebook, Twitter, Instagram, LinkedIn, and Snapchat.';
            case 'History of social media':
                return 'Social media has evolved from early platforms like MySpace to modern platforms like Instagram and TikTok.';
            case 'Key milestones in social media evolution':
                return 'Key milestones include the launch of Facebook, the rise of mobile apps, and the advent of short-form video content.';
            case 'Recent developments in social media':
                return 'Recent developments include the integration of artificial intelligence, increased focus on privacy, and the growth of new social media platforms.';
            case 'Personal benefits':
                return 'Social media helps in staying connected with friends and family, finding community support, and sharing personal achievements.';
            case 'Professional benefits':
                return 'It offers networking opportunities, a platform for brand building, and tools for customer engagement.';
            case 'Privacy concerns':
                return 'Privacy concerns include data breaches, unauthorized access to personal information, and data misuse.';
            case 'Cyberbullying and misinformation':
                return 'Social media can be a platform for cyberbullying and spreading false information, which can have serious consequences.';
            case 'Creating engaging content':
                return 'Focus on high-quality visuals, interactive posts, and relevant content to engage your audience.';
            case 'Managing your online presence':
                return 'Regularly update your profiles, interact with followers, and maintain a consistent brand voice.';
            case 'Short-form videos':
                return 'Short-form videos, such as those on TikTok and Instagram Reels, are becoming increasingly popular for engaging content.';
            case 'Augmented reality':
                return 'Augmented reality is being used in social media to create immersive experiences and interactive content.';
            default:
                return 'I am not sure about that. Can you please choose one of the provided options?';
        }
    };

    return (
        <div className="min-h-screen flex items-center justify-center bg-darkSecondary ">
            <div className="w-full max-w-5xl bg-darkPrimary border border-gray-700 rounded-lg shadow-lg flex flex-col h-[80vh] md:h-[70vh] lg:h-[80vh]">
                <div className="flex-1 p-4 overflow-y-auto">
                    {messages.map((msg, index) => (
                        <div
                            key={index}
                            className={`p-3 my-2 rounded-lg max-w-lg break-words ${msg.isUser ? 'bg-icon text-white ml-auto' : 'bg-gray-700 text-white mr-auto'}`}
                            style={{ animation: msg.isUser ? 'fadeInRight 0.5s' : 'fadeInLeft 0.5s' }}
                        >
                            {msg.text}
                        </div>
                    ))}
                    {isTyping && (
                        <div className="flex items-center space-x-2">
                            <div className="w-2.5 h-2.5 bg-gray-400 rounded-full animate-bounce"></div>
                            <div className="w-2.5 h-2.5 bg-gray-400 rounded-full animate-bounce delay-200"></div>
                            <div className="w-2.5 h-2.5 bg-gray-400 rounded-full animate-bounce delay-400"></div>
                        </div>
                    )}
                    <div ref={messagesEndRef} />
                </div>
                <div className="min-h-32 p-4 border-t border-gray-700 bg-darkPrimary">
                    <div className="grid grid-cols-2 sm:grid-cols-3 md:grid-cols-4 lg:grid-cols-6 gap-2">
                        {subOptions.length > 0 ? (
                            subOptions.map((subOption, index) => (
                                <button
                                    key={index}
                                    onClick={() => handleSelectOption(subOption)}
                                    className={`flex-none p-2 md:p-3 lg:p-4 bg-icon text-white rounded-lg hover:bg-icon1 ${isCooldown ? 'cursor-not-allowed opacity-50' : ''}`}
                                    disabled={isCooldown}
                                >
                                    {subOption}
                                </button>
                            ))
                        ) : (
                            options.map((option, index) => (
                                <button
                                    key={index}
                                    onClick={() => handleSelectOption(option)}
                                    className={`flex-none p-2 md:p-3 lg:p-4 bg-icon text-white rounded-lg hover:bg-icon1 ${isCooldown ? 'cursor-not-allowed opacity-50' : ''}`}
                                    disabled={isCooldown}
                                >
                                    {option}
                                </button>
                            ))
                        )}
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ChatBot;
