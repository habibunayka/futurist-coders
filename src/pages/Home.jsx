import React, { useState } from "react";
import { FaUsers, FaShareAlt, FaUsersCog, FaBrain } from "react-icons/fa";
import { Link } from "react-router-dom";
import "../styles/css/custom.css";

const Home = () => {
    const [openFAQ, setOpenFAQ] = useState(null);

    const toggleFAQ = (index) => {
        setOpenFAQ(openFAQ === index ? null : index);
    };

    return (
        <div className="scrollbar font-helvetica">
            {/* Hero Section */}
            <section
                id="home"
                className="relative flex items-center justify-center min-h-screen bg-darkPrimary text-white overflow-hidden"
            >
                <div className="absolute inset-0 opacity-20">
                    <img
                        src="https://www.moxeemarketing.com/wp-content/uploads/2022/12/Logos-for-a-wide-range-of-social-media-platforms.jpg"
                        className="w-full h-full object-cover object-center"
                        alt="Social Media Technology"
                    />
                    <div className="absolute inset-0 bg-gradient-to-t from-black via-transparent to-transparent opacity-50"></div>
                </div>
                <div className="relative z-10 text-center px-4 md:px-8 lg:px-16">
                    <h1 className="  text-5xl md:text-7xl font-bold leading-tight mb-6">
                        <span className="text-icon">Futuristic</span> Coders
                    </h1>
                    <h3 className="text-3xl md:text-5xl font-bold leading-tight mb-6">
                        Exploring Social Media Technology
                    </h3>
                    <p className="text-xl md:text-2xl mb-8">
                        Discover the latest advancements and the impact of
                        technology on social media platforms.
                    </p>
                    <a
                        href="#main-content"
                        className="bg-icon text-white text-lg font-semibold px-8 py-4 rounded-full shadow-lg hover:bg-icon1 transition-colors"
                    >
                        Learn More
                    </a>
                </div>
            </section>

            {/* Why Social Media? */}
            <section
                id="why-social-media"
                className="bg-darkSecondary text-white flex flex-col md:flex-row items-center py-12 min-h-screen"
            >
                <div className="w-full md:w-1/2">
                    <div
                        className="relative"
                        style={{ paddingBottom: "56.25%" }}
                    >
                        <img
                            src="https://images.pexels.com/photos/1181242/pexels-photo-1181242.jpeg"
                            className="absolute top-0 left-0 w-full h-full object-cover rounded-lg shadow-lg"
                            alt="Why Social Media?"
                        />
                    </div>
                </div>
                <div className="w-full md:w-1/2 text-center mt-16 md:mt-0 md:text-left md:pl-8">
                    <h2 className="text-3xl font-bold mb-4">
                        Why Social Media?
                    </h2>
                    <p className="text-lg leading-8">
                        Social media has become an integral part of daily life.
                        By leveraging the latest technology, we can understand
                        how social media influences our communication,
                        information sharing, and community building. Social
                        media allows us to connect with a global audience,
                        enhance engagement, and expand our networks.
                    </p>
                </div>
            </section>

            {/* Social Media Development */}
            <section
                id="additional-questions"
                className="bg-darkPrimary text-white flex flex-col md:flex-row items-center py-12 min-h-screen"
            >
                <div className="w-full md:w-1/2 text-center md:text-right md:pr-8">
                    <h2 className="text-3xl font-bold mb-4">
                        Social Media Development
                    </h2>
                    <p className="text-lg leading-8">
                        The development of social media has transformed how we
                        communicate and share information. By leveraging the
                        latest technology, we can understand how social media
                        influences our interactions, information dissemination,
                        and community building. Social media allows us to
                        connect with a global audience, enhance engagement, and
                        expand our networks.
                    </p>
                </div>
                <div className="w-full md:w-1/2 mt-16 md:mt-0">
                    <div
                        className="relative"
                        style={{ paddingBottom: "56.25%" }}
                    >
                        <img
                            src="https://reviewkita.com/wp-content/uploads/2023/03/pertarungan-media-konvensional-di-era-serba-media-sosial-pe2Hh1ialO.jpg"
                            className="absolute top-0 left-0 w-full h-full object-cover rounded-lg shadow-lg"
                            alt="Why Social Media?"
                        />
                    </div>
                </div>
            </section>

            {/* Social Media Impact */}
            <section className="bg-darkSecondary text-white py-12 min-h-screen flex items-center">
                <div className="container mx-auto px-4">
                    <h2 className="text-3xl font-bold text-center mb-8">
                        The Impact of Social Media
                    </h2>
                    <p className="text-lg leading-8 text-center mb-8">
                        Social media has transformed the way we interact and
                        connect with the world. Here are some key impacts of
                        social media on society:
                    </p>
                    <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-6 max-w-5xl mx-auto">
                        <div className="bg-primary p-6 rounded-lg shadow-lg hover:scale-105 transition-transform text-center">
                            <FaUsers className="text-4xl mb-4 mx-auto text-icon" />
                            <h3 className="text-xl font-semibold">
                                Enhanced Connectivity
                            </h3>
                            <p className="mt-2 text-gray-300">
                                Social media allows users to connect with people
                                from different corners of the world with ease.
                            </p>
                        </div>
                        <div className="bg-primary p-6 rounded-lg shadow-lg hover:scale-105 transition-transform text-center">
                            <FaShareAlt className="text-4xl mb-4 mx-auto text-icon" />
                            <h3 className="text-xl font-semibold">
                                Rapid Information Spread
                            </h3>
                            <p className="mt-2 text-gray-300">
                                Information and news can spread quickly through
                                social media platforms, influencing public
                                opinion.
                            </p>
                        </div>
                        <div className="bg-primary p-6 rounded-lg shadow-lg hover:scale-105 transition-transform text-center">
                            <FaUsersCog className="text-4xl mb-4 mx-auto text-icon" />
                            <h3 className="text-xl font-semibold">
                                Community Engagement
                            </h3>
                            <p className="mt-2 text-gray-300">
                                Social media facilitates the creation of
                                communities and groups with shared interests.
                            </p>
                        </div>
                        <div className="bg-primary p-6 rounded-lg shadow-lg hover:scale-105 transition-transform text-center">
                            <FaBrain className="text-4xl mb-4 mx-auto text-icon" />
                            <h3 className="text-xl font-semibold">
                                Mental Health Impacts
                            </h3>
                            <p className="mt-2 text-gray-300">
                                Social media usage can affect mental health,
                                both positively and negatively.
                            </p>
                        </div>
                    </div>
                </div>
            </section>

            {/* Latest Articles */}
            <section
                id="articles"
                className="bg-darkSecondary text-white py-12"
            >
                <div className="container mx-auto px-4">
                    <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-6">
                        {/* Article 1 */}
                        <div className="bg-primary p-6 rounded-lg shadow-lg">
                            <img
                                src="https://quickframe.com/wp-content/uploads/2023/06/QF-Blog-How-Do-Social-Media-Algorithms-Work-in-2023-01.jpg"
                                alt="Understanding Social Media Algorithms"
                                className="w-full h-40 object-cover rounded-t-lg mb-4"
                            />
                            <h3 className="text-xl font-semibold mb-4">
                                Understanding Social Media Algorithms
                            </h3>
                            <p className="text-gray-300 mb-4">
                                Dive into the mechanics behind social media
                                algorithms and how they influence content
                                visibility.
                            </p>
                            <Link
                                to="/articles/understanding-social-media-algorithms"
                                className="text-icon font-semibold hover:underline"
                            >
                                Read More
                            </Link>
                        </div>
                        {/* Article 2 */}
                        <div className="bg-primary p-6 rounded-lg shadow-lg">
                            <img
                                src="https://i0.wp.com/kliklegal.com/wp-content/uploads/2021/09/Perbedaan-Data-Privacy-dan-Data-Protection-serta-Pengaturannya-pada-RUU-Pelindungan-Data-Pribadi.jpg?fit=700%2C386&ssl=1"
                                alt="The Role of Data Privacy in Social Media"
                                className="w-full h-40 object-cover rounded-t-lg mb-4"
                            />
                            <h3 className="text-xl font-semibold mb-4">
                                The Role of Data Privacy in Social Media
                            </h3>
                            <p className="text-gray-300 mb-4">
                                Explore the importance of data privacy and the
                                measures you can take to protect your personal
                                information.
                            </p>
                            <Link
                                to="/articles/the-role-of-data-privacy"
                                className="text-icon font-semibold hover:underline"
                            >
                                Read More
                            </Link>
                        </div>
                        {/* Article 3 */}
                        <div className="bg-primary p-6 rounded-lg shadow-lg">
                            <img
                                src="https://www.socialchamp.io/wp-content/uploads/2023/11/Content-Blog-Banner_Q3-2023_1125x600_73-Social-Media-Platforms.png"
                                alt="The Evolution of Social Media Platforms"
                                className="w-full h-40 object-cover rounded-t-lg mb-4"
                            />
                            <h3 className="text-xl font-semibold mb-4">
                                The Evolution of Social Media Platforms
                            </h3>
                            <p className="text-gray-300 mb-4">
                                A look at the evolution of social media
                                platforms from their inception to the present
                                day.
                            </p>
                            <Link
                                to="/articles/the-evolution-of-social-media-platforms"
                                className="text-icon font-semibold hover:underline"
                            >
                                Read More
                            </Link>
                        </div>
                    </div>
                </div>
            </section>

            {/* Frequently Asked Questions */}
            <section
                id="faq"
                className="bg-darkPrimary text-white py-12 px-4 min-h-screen"
            >
                <div className="container mx-auto">
                    <h2 className="text-3xl font-bold text-center mb-8">
                        Frequently Asked Questions
                    </h2>
                    <div className="space-y-6">
                        {[
                            {
                                question: "What is Social Media?",
                                answer: "Social media refers to websites and applications that enable users to create and share content or to participate in social networking. Social media platforms include popular sites such as Facebook, Twitter, Instagram, and LinkedIn. They allow users to connect with friends, family, and other users around the world. Through these platforms, users can post updates, share photos and videos, and engage in discussions on a wide range of topics. The influence of social media extends to various aspects of life, including communication, marketing, and entertainment.",
                            },
                            {
                                question:
                                    "How Does Technology Affect Social Media?",
                                answer: "Technology plays a crucial role in shaping the functionality and experience of social media platforms. Advances in technology, such as improvements in artificial intelligence, machine learning, and data analytics, have enabled more sophisticated algorithms for content recommendation and personalization. Enhanced technology also supports higher-quality multimedia content, faster loading times, and more interactive features. Moreover, technological innovations drive the development of new social media tools and platforms, influencing how users interact and engage with content. As technology evolves, social media continues to adapt, offering new ways for users to connect and communicate.",
                            },
                            {
                                question:
                                    "Why is it Important to Understand the Impact of Social Media?",
                                answer: "Understanding the impact of social media is essential for several reasons. First, it helps individuals and organizations navigate the complexities of online interactions and content sharing. By comprehending the effects of social media on mental health, privacy, and social dynamics, users can make informed decisions about their online behavior and interactions. Second, understanding social media impact allows businesses to develop effective strategies for digital marketing, customer engagement, and brand management. Lastly, being aware of the influence of social media on public opinion and societal trends can help policymakers and educators address challenges and leverage opportunities in the digital age.",
                            },
                            {
                                question:
                                    "How Has Social Media Evolved Over the Years?",
                                answer: "Social media has undergone significant evolution since its inception. Initially, social media platforms were primarily used for basic networking and communication, such as sharing updates and photos. Over time, these platforms have integrated advanced features like live streaming, stories, and augmented reality filters. The rise of mobile technology and apps has further accelerated this evolution, enabling users to access and interact with social media on the go. Additionally, the focus has shifted towards creating more engaging and interactive experiences, with platforms investing in artificial intelligence to enhance content recommendations and user interactions. The evolution of social media reflects changing user behaviors and technological advancements, shaping how we connect and communicate online.",
                            },
                            {
                                question:
                                    "How Has Social Media Influenced Modern Communication?",
                                answer: "Social media has profoundly influenced modern communication by making it faster and more accessible. Platforms like WhatsApp and Messenger have replaced traditional texting and calling methods with instant messaging and multimedia sharing. Social media has also facilitated real-time interactions through live streaming and video calls, breaking down geographical barriers. The widespread use of hashtags and trends has enabled people to join global conversations and participate in social movements. Additionally, social media has introduced new forms of communication, such as memes and emojis, that reflect and shape contemporary digital culture. The influence of social media on communication continues to evolve as new technologies and platforms emerge.",
                            },
                            {
                                question:
                                    "What Are the Benefits of Social Media for Businesses?",
                                answer: "Social media offers numerous benefits for businesses. It provides a platform for brand promotion and customer engagement, allowing companies to reach a large and diverse audience. Through social media, businesses can create targeted advertising campaigns, gather insights into customer preferences, and build brand loyalty. Social media also facilitates real-time interaction with customers, enabling businesses to address inquiries, resolve issues, and receive feedback promptly. Additionally, social media platforms can serve as valuable tools for market research, helping businesses understand trends, analyze competition, and identify new opportunities. Leveraging social media effectively can enhance a company's online presence and contribute to overall business growth and success.",
                            },
                        ].map((item, index) => (
                            <div
                                key={index}
                                className="bg-primary p-6 rounded-lg shadow-lg"
                            >
                                <button
                                    className="w-full text-left flex items-center justify-between focus:outline-none"
                                    onClick={() => toggleFAQ(index)}
                                >
                                    <h3 className="text-xl font-semibold">
                                        {item.question}
                                    </h3>
                                    <i
                                        className={`fas fa-chevron-down transition-transform ${
                                            openFAQ === index
                                                ? "rotate-180"
                                                : ""
                                        }`}
                                    ></i>
                                </button>
                                <div
                                    className={`mt-2 text-gray-300 overflow-hidden transition-max-height duration-500 ease-in-out ${
                                        openFAQ === index
                                            ? "max-h-96"
                                            : "max-h-0"
                                    }`}
                                >
                                    {item.answer}
                                </div>
                            </div>
                        ))}
                    </div>
                </div>
            </section>

            {/*  */}
            <section
                id="chatbot-promo"
                className="bg-icon min-h-screen flex flex-col justify-between text-darkPrimary py-12"
            >
                <div className="container mx-auto px-4 text-center items-center my-auto">
                    <i
                        className="bx bx-bot text-7xl"
                        style={{ color: "#232428" }}
                    ></i>
                    <h2 className="text-3xl font-bold mb-6 mt-2">
                        Ask More? Meet Our Chatbot!
                    </h2>
                    <p className="text-lg leading-8 px-4 mt-4 mb-10">
                        Our chatbot is here to assist you with any questions you
                        might have. It's designed to provide you with the
                        information you need quickly and efficiently. Give it a
                        try and see how it can enhance your experience!
                    </p>
                    <Link
                        to="/chatbot"
                        className="bg-darkPrimary text-white text-lg font-semibold px-8 py-4 rounded-full shadow-lg hover:bg-darkSecondary transition-colors"
                    >
                        Try the Chatbot
                    </Link>
                </div>
                <div className="container mx-auto text-center mt-4">
                    <p>
                        &copy; 2024 Social Media Technology Website. All rights
                        reserved.
                    </p>
                </div>
            </section>
        </div>
    );
};

export default Home;
