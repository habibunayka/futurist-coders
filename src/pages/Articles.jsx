import React from "react";
import { Link } from "react-router-dom";

const Articles = () => {
    return (
        <div className="scrollbar font-helvetica bg-darkPrimary text-white">
            {/* Hero Section */}
            <section className="relative flex items-center justify-center min-h-screen bg-darkPrimary text-white overflow-hidden py-12">
                <img
                    src="https://artamaya.web.id/wp-content/uploads/2023/02/sosial-media-untuk-seo.jpg"
                    alt="Background"
                    className="absolute inset-0 w-full h-full object-cover z-0"
                />
                <div className="absolute inset-0 bg-black opacity-70 z-0"></div>
                <div className="relative z-10 text-center px-4 md:px-8 lg:px-16">
                    <h1 className="text-4xl md:text-5xl font-bold leading-tight mb-6">
                        Articles on Social Media Technology
                    </h1>
                    <p className="text-xl md:text-2xl mb-8">
                        Stay updated with in-depth articles on how technology is
                        shaping social media.
                    </p>
                </div>
            </section>

            {/* Articles List */}
            <section
                id="articles"
                className="bg-darkSecondary text-white py-12"
            >
                <div className="container mx-auto px-4">
                    <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-6">
                        {/* Article 1 */}
                        <div className="bg-primary p-6 rounded-lg shadow-lg">
                            <img
                            src="https://quickframe.com/wp-content/uploads/2023/06/QF-Blog-How-Do-Social-Media-Algorithms-Work-in-2023-01.jpg"
                                alt="Understanding Social Media Algorithms"
                                className="w-full h-48 object-cover rounded-md mb-4"
                            />
                            <h3 className="text-xl font-semibold mb-4">
                                Understanding Social Media Algorithms
                            </h3>
                            <p className="text-gray-300 mb-4">
                                Dive into the mechanics behind social media
                                algorithms and how they influence content
                                visibility. Learn about the different types of
                                algorithms used by various platforms and their
                                impact on user engagement and content reach.
                            </p>
                            <Link
                                to="/articles/understanding-social-media-algorithms"
                                className="text-icon font-semibold hover:underline"
                            >
                                Read More
                            </Link>
                        </div>
                        {/* Article 2 */}
                        <div className="bg-primary p-6 rounded-lg shadow-lg">
                            <img
                                src="https://i0.wp.com/kliklegal.com/wp-content/uploads/2021/09/Perbedaan-Data-Privacy-dan-Data-Protection-serta-Pengaturannya-pada-RUU-Pelindungan-Data-Pribadi.jpg?fit=700%2C386&ssl=1"
                                alt="The Role of Data Privacy in Social Media"
                                className="w-full h-48 object-cover rounded-md mb-4"
                            />
                            <h3 className="text-xl font-semibold mb-4">
                                The Role of Data Privacy in Social Media
                            </h3>
                            <p className="text-gray-300 mb-4">
                                Explore the importance of data privacy in the
                                context of social media. This article covers
                                essential strategies for safeguarding your
                                personal information and understanding the
                                privacy policies of major social media
                                platforms.
                            </p>
                            <Link
                                to="/articles/the-role-of-data-privacy"
                                className="text-icon font-semibold hover:underline"
                            >
                                Read More
                            </Link>
                        </div>
                        {/* Article 3 */}
                        <div className="bg-primary p-6 rounded-lg shadow-lg">
                            <img
                                src="https://www.socialchamp.io/wp-content/uploads/2023/11/Content-Blog-Banner_Q3-2023_1125x600_73-Social-Media-Platforms.png"
                                alt="The Evolution of Social Media Platforms"
                                className="w-full h-48 object-cover rounded-md mb-4"
                            />
                            <h3 className="text-xl font-semibold mb-4">
                                The Evolution of Social Media Platforms
                            </h3>
                            <p className="text-gray-300 mb-4">
                                A comprehensive look at the evolution of social
                                media platforms. From the early days of simple
                                message boards to today's complex ecosystems,
                                this article examines how social media has
                                evolved and its future directions.
                            </p>
                            <Link
                                to="/articles/the-evolution-of-social-media-platforms"
                                className="text-icon font-semibold hover:underline"
                            >
                                Read More
                            </Link>
                        </div>
                    </div>
                </div>
            </section>

            {/* Footer */}
            <footer className="bg-darkSecondary text-white py-4">
                <div className="container mx-auto text-center">
                    <p>
                        &copy; 2024 Social Media Technology Website. All rights
                        reserved.
                    </p>
                </div>
            </footer>
        </div>
    );
};

export default Articles;
