import React, { useState } from "react";
import emailjs from "emailjs-com";

const About = () => {
    const [formData, setFormData] = useState({
        name: "",
        email: "",
        message: "",
    });
    const [status, setStatus] = useState("");

    const handleChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value,
        });
    };

    const handleSubmit = (e) => {
        e.preventDefault();

        emailjs
            .sendForm(
                "service_5h5b8qg",
                "template_g6x1wfh",
                e.target,
                "2Sfpi-OXXGC4LnTPP"
            )
            .then(
                (result) => {
                    setStatus("Message sent successfully!");
                },
                (error) => {
                    setStatus("Failed to send message.");
                }
            );
    };

    return (
        <div className="font-helvetica">
            {/* Hero Section */}
            <section className="relative flex items-center justify-center min-h-screen bg-black text-white overflow-hidden py-16">
    <img
        src="https://images.tech.co/wp-content/uploads/2022/10/14123538/socialmedia-1-1024x512.jpg"
        alt="Background"
        className="absolute inset-0 w-full h-full object-cover brightness-70 z-0"
    />
    <div className="absolute inset-0 bg-gradient-to-r from-darkPrimary to-transparent opacity-50 z-10"></div>
    <div className="relative z-20 text-center px-6 md:px-12 lg:px-24">
        <h1 className="text-4xl md:text-5xl lg:text-6xl font-extrabold mb-8 md:mb-12 leading-tight text-white shadow-md">
            Twin Futuristic Coders
        </h1>
        <p className="text-base md:text-lg lg:text-xl leading-relaxed text-white max-w-3xl mx-auto">
            We are Twin Futuristic Coders, a forward-thinking team committed to pushing the boundaries of social media technology. We are a group of passionate innovators dedicated to creating transformative digital solutions that enhance how people connect and engage online.
        </p>
    </div>
</section>


            {/* About Us */}
            <section
                id="about-us"
                className="bg-darkSecondary text-white py-20 h-screen flex items-center justify-center"
            >
                <div className="max-w-4xl text-center px-4">
                    <h2 className="text-4xl font-bold mb-12">
                        Our Mission & Vision
                    </h2>
                    <div className="flex flex-col md:flex-row justify-center gap-8">
                        {/* Mission Box */}
                        <div className="bg-primary p-8 rounded-lg shadow-lg transform hover:scale-105 transition-transform duration-300 w-full md:w-1/2 relative">
                            <div className="absolute top-[-30px] left-1/2 transform -translate-x-1/2 bg-icon p-4 rounded-full">
                                <i className='bx bx-bullseye text-4xl text-white'></i>
                            </div>
                            <h3 className="text-2xl font-semibold mb-4 mt-10">
                                Our Mission
                            </h3>
                            <p className="text-gray-300">
                                Our mission is to advance the understanding of
                                social media technology and its impact on
                                society. We aim to provide insightful analyses
                                and foster meaningful discussions.
                            </p>
                        </div>
                        {/* Vision Box */}
                        <div className="bg-primary p-8 rounded-lg shadow-lg transform hover:scale-105 transition-transform duration-300 w-full md:w-1/2 relative">
                            <div className="absolute top-[-30px] left-1/2 transform -translate-x-1/2 bg-icon p-4 rounded-full">
                                <i className='bx bx-globe text-4xl text-white'></i>
                            </div>
                            <h3 className="text-2xl font-semibold mb-4 mt-10">
                                Our Vision
                            </h3>
                            <p className="text-gray-300">
                                We envision a future where technology enhances
                                our ability to connect and engage with each
                                other in a positive and impactful way.
                            </p>
                        </div>
                    </div>
                </div>
            </section>

            {/* Our Team */}
            <section
                id="our-team"
                className="bg-gradient-to-r from-darkPrimary to-darkSecondary text-white py-20 h-screen flex items-center mt-16 md:mt-0 justify-center"
            >
                <div className="max-w-4xl text-center px-4">
                    <h2 className="text-4xl font-bold mb-12">Meet the Team</h2>
                    <div className="flex flex-col md:flex-row justify-center gap-8">
                        <div className="bg-primary p-6 rounded-lg shadow-2xl flex flex-col items-center transform hover:scale-105 transition-transform duration-300 w-full md:w-1/2">
                            <div className="mb-4">
                                <img
                                    src="./assets/habibunayka.png"
                                    alt="Habibunayka Miftah Al-Rizqi"
                                    className="w-32 h-32 rounded-full object-cover transition-transform duration-300 hover:scale-105"
                                />
                            </div>
                            <div>
                                <h3 className="text-xl font-semibold mb-2">
                                    Habibunayka Miftah Al-Rizqi
                                </h3>
                                <p className="text-gray-300">
                                    Lead Developer. Passionate about creating
                                    innovative solutions and exploring new
                                    technologies.
                                </p>
                            </div>
                        </div>
                        <div className="bg-primary p-6 rounded-lg shadow-2xl flex flex-col items-center transform hover:scale-105 transition-transform duration-300 w-full md:w-1/2">
                            <div className="mb-4">
                                <img
                                    src="./assets/ridwan.png"
                                    alt="Ridwan Adiansyah"
                                    className="w-32 h-32 rounded-full object-cover transition-transform duration-300 hover:scale-105"
                                />
                            </div>
                            <div>
                                <h3 className="text-xl font-semibold mb-2">
                                    Ridwan Adiansyah
                                </h3>
                                <p className="text-gray-300">
                                    UX/UI Designer. Dedicated to designing
                                    user-friendly interfaces and enhancing user
                                    experiences.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            

            {/* Contact Us */}
            <section
                id="contact"
                className="bg-darkSecondary text-white py-20 h-screen flex items-center justify-center"
            >
                <div className="max-w-4xl text-center px-4">
                    <h2 className="text-4xl font-bold mb-12">Contact Us</h2>
                    <form
                        onSubmit={handleSubmit}
                        className="max-w-lg mx-auto space-y-6"
                    >
                        <input
                            type="text"
                            name="name"
                            placeholder="Your Name"
                            value={formData.name}
                            onChange={handleChange}
                            required
                            className="w-full p-4 rounded bg-darkPrimary text-white border border-gray-600 placeholder-gray-400"
                        />
                        <input
                            type="email"
                            name="email"
                            placeholder="Your Email"
                            value={formData.email}
                            onChange={handleChange}
                            required
                            className="w-full p-4 rounded bg-darkPrimary text-white border border-gray-600 placeholder-gray-400"
                        />
                        <textarea
                            name="message"
                            placeholder="Your Message"
                            value={formData.message}
                            onChange={handleChange}
                            required
                            rows="4"
                            className="w-full p-4 rounded bg-darkPrimary text-white border border-gray-600 placeholder-gray-400"
                        ></textarea>
                        <button
                            type="submit"
                            className="bg-icon text-white text-sm font-semibold px-6 py-3 rounded shadow-md hover:bg-icon1 transition-colors"
                        >
                            Send Message
                        </button>
                        {status && <p className="mt-4 text-lg">{status}</p>}
                    </form>
                </div>
            </section>

            <footer className="bg-darkSecondary text-white py-4">
                <div className="container mx-auto text-center">
                    <p>
                        &copy; 2024 Social Media Technology Website. All rights
                        reserved.
                    </p>
                </div>
            </footer>
        </div>
    );
};

export default About;
