import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import "./styles/css/custom.css";
import App from "./App";
import { BrowserRouter as Router } from "react-router-dom";
import AOS from "aos";
import "aos/dist/aos.css";
import '@fortawesome/fontawesome-free/css/all.min.css';
import './styles/js/script';


AOS.init();

ReactDOM.render(
    <Router>
        <React.StrictMode>
            <App />
        </React.StrictMode>
    </Router>,
    document.getElementById("root")
);
