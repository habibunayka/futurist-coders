import React, { useEffect } from 'react'; // Import useEffect
import { Route, Routes, useLocation } from 'react-router-dom';
import Sidebar from './components/Sidebar';
import Home from './pages/Home';
import About from './pages/About';
import Articles from './pages/Articles';
import ArticleDetails from './pages/ArticleDetails';
import ChatBot from './pages/ChatBot';
import 'aos/dist/aos.css';
import '@fortawesome/fontawesome-free/css/all.min.css';
import './styles/css/custom.css';
import './styles/js/script';

function App() {
  const location = useLocation();

  // Determine if the current path is the ChatBot page
  const isChatBotPage = location.pathname === '/chatbot';

  useEffect(() => {
    // Scroll to top on route change
    window.scrollTo(0, 0);
  }, [location]);

  return (
    <div className={`scrollbar flex md:w-[199vh] md:h-[98vh] h-screen md:m-[1vh] p-0 ${isChatBotPage ? 'overflow-hidden' : ''}`} style={{ borderRadius: '1vh' }}>
      <Sidebar />
      <main className={`flex-1 p-2 bg-darkSecondary text-white ${isChatBotPage ? 'overflow-hidden' : 'overflow-y-auto'}`}>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/articles" element={<Articles />} />
          <Route path="/about" element={<About />} />
          <Route path="/chatbot" element={<ChatBot />} />
          <Route path="/articles/:articleId" element={<ArticleDetails />} />
        </Routes>
      </main>
    </div>
  );
}

export default App;
